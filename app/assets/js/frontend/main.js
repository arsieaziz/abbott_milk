$(document).ready(function(){

	AOS.init({
		duration: 1200
	});

	var iCarousel = 0;
	$('.icon-item').click(function(){
		var title = $(this).data('title');
		var description = $(this).data('description');
		$(this).closest('.icon-composition').find('.active').removeClass('active');
		$(this).closest('.icon-container').addClass('active');
		$('.main-composition > .sub-title').removeClass('active');
		setTimeout(function(){
			$('#titleComposition').html(title);
			$('.main-composition > .sub-title').addClass('active');
		},500);
		$('.description-composition > .card-description').removeClass('active');
		setTimeout(function(){
			$('#descriptionComposition').html(description);
			$('.description-composition > .card-description').addClass('active');
		},500);
		var itung = $(this).closest('.icon-container').data('count');
		if (itung > 8) {
			iCarousel = 0;
		}else{
			iCarousel = (itung + 1);
		}
	});

	setInterval(function(){
		if (iCarousel == 0) {
			var dataActive = $('.icon-composition').find('.active');
			dataActive.removeClass('active');
			$('.icon-composition > .icon-container').first().addClass('active');
			var title = $('.icon-composition > .icon-container').first().find('.icon-item').data('title');
			var description = $('.icon-composition > .icon-container').first().find('.icon-item').data('description');
			$('.main-composition > .sub-title').removeClass('active');
			setTimeout(function(){
				$('#titleComposition').html(title);
				$('.main-composition > .sub-title').addClass('active');
			},500);
			$('.description-composition > .card-description').removeClass('active');
			setTimeout(function(){
				$('#descriptionComposition').html(description);
				$('.description-composition > .card-description').addClass('active');
			},500);
		}else{
			var dataActive = $('.icon-composition').find('.active');
			dataActive.next().addClass('active');
			var title = dataActive.next().find('.icon-item').data('title');
			var description = dataActive.next().find('.icon-item').data('description');
			dataActive.removeClass('active');
			$('.main-composition > .sub-title').removeClass('active');
			setTimeout(function(){
				$('#titleComposition').html(title);
				$('.main-composition > .sub-title').addClass('active');
			},500);
			$('.description-composition > .card-description').removeClass('active');
			setTimeout(function(){
				$('#descriptionComposition').html(description);
				$('.description-composition > .card-description').addClass('active');
			},500);
		}
		iCarousel++;
		var totalList = $('.icon-composition > .icon-container').length;
		if (iCarousel == totalList) {
			iCarousel = 0;
		}
	},5000);


	$(window).scroll(function() {
		$('.video').each( function(i){
			var position = $(window).scrollTop();
			var bottom_element = $(this).offset().top + ($(this).outerHeight() / 2);
			var bottom_window = $(window).scrollTop() + $(window).height();
			if( bottom_window > bottom_element && position < bottom_element ) {
				$('iframe#youtubeVideo')[0].contentWindow.postMessage('{"event":"command","func":"' + 'playVideo' + '","args":""}', '*');
			} else {
				$('iframe#youtubeVideo')[0].contentWindow.postMessage('{"event":"command","func":"' + 'pauseVideo' + '","args":""}', '*');
			}
	    });

	    $('.shop').each( function(i){
			var positionShop = $(window).scrollTop();
			var bottom_elementShop = $(this).offset().top + ($(this).outerHeight() / 2);
			var bottom_windowShop = $(window).scrollTop() + $(window).height();
			if( bottom_windowShop > bottom_elementShop && positionShop < bottom_elementShop ) {
				$('.shop-float').removeClass('active');
			} else {
				$('.shop-float').addClass('active');
			}
	    });


	    if ($(this).scrollTop() >= 150) {
	        $('#return-to-top').addClass('active');
	    } else {
	        $('#return-to-top').removeClass('active');
	    }
	});

	$('.shop-float').click(function() {
	    $('html, body').animate({
	        scrollTop: $('.shop').offset().top
	    }, 2000);
	});

	$('#return-to-top').click(function(e) {
		e.preventDefault();
	    $('body,html').animate({
	        scrollTop : 0 }, 1000);
	});

	setTimeout(function(){
		var dataActive = $('.icon-composition').find('.active');
		dataActive.removeClass('active');
		$('.icon-composition > .icon-container').first().addClass('active');
		var title = $('.icon-composition > .icon-container').first().find('.icon-item').data('title');
		var description = $('.icon-composition > .icon-container').first().find('.icon-item').data('description');
		$('.main-composition > .sub-title').removeClass('active');
		setTimeout(function(){
			$('#titleComposition').html(title);
			$('.main-composition > .sub-title').addClass('active');
		},500);
		$('.description-composition > .card-description').removeClass('active');
		setTimeout(function(){
			$('#descriptionComposition').html(description);
			$('.description-composition > .card-description').addClass('active');
		},500);
		iCarousel = 1;
	},500);
});